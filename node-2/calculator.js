const fs = require('fs');

function sumar(a,b){
    return a + b;
}

function restar(a,b){
    return a - b;
}

function multiplicar(a,b){
    return a * b;
}

function dividir(a,b){
    while (a != 0 && b != 0 ){
    return a / b; 
    }
    return "No se puede dividir por cero"
}


function exportCuenta(cuenta){
    fs.appendFile("./calc.txt", `${cuenta} \n\r`, function(err)  {
        if(err) {
        return console.log(err);
     }
        console.log("El archivo se guardo!");
    }); 
return cuenta
}


module.exports = {sumar, restar, multiplicar, dividir, exportCuenta};
